#!/bin/bash

# this script creates vm templates based on cloud init images
# from ubuntu and centos
# usage: ./create-vm-template.sh --vmid <number> --storage local-lvm --os ubuntu

UBUNTU_2004_URL=https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img
CENTOS_URL=https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.4.2105-20210603.0.x86_64.qcow2

ARGS=$(getopt -a --options i:s:o --long "vmid:,storage:,os:" -- "$@")
eval set -- "$ARGS"

usage() {
  echo "Usage: $0 -i <number> -s <storage-name> -o <ubuntu|centos>" 1>&2; exit 1;
}

while true; do
  case "$1" in
    --vmid)
      vmid="$2"
      shift 2;;
    --storage)
      storage="$2"
      shift 2;;
    --os)
      os="$2"
      shift 2
      break;;
    --)
      usage
  esac
done

check_deps() {
  if ! command -v $1 &> /dev/null
  then
    echo "$1 command doesn't exist, installing it.."
    apt install libguestfs-tools
  fi
}

pull_image() {
  if [ $1 == 'ubuntu' ]
  then
    echo "downloading cloud-init image for $1"
    curl $UBUNTU_2004_URL -o $2
  elif [ $1 == 'centos' ]
  then
    echo "still in development for centos image"
    exit 0
  fi
}

customize() {
  echo "customizing image $1"
  virt-customize --install qemu-guest-agent -a $1
  virt-customize --install net-tools -a $1
  virt-customize -a $1 --run-command "systemctl enable qemu-guest-agent"
}

create_template() {
  echo "creating template $4"
  qm create $1 --memory 4096 --net0 virtio,bridge=vmbr0
  qm importdisk $1 $2 $3
  qm set $1 --scsihw virtio-scsi-pci --scsi0 $3:vm-$1-disk-0
  qm set $1 --agent enabled=1
  qm set $1 --ide2 local-lvm:cloudinit
  qm set $1 --boot c --bootdisk scsi0
  qm set $1 --serial0 socket --vga serial0
  qm set $1 --description "Created at $(date)"
  qm template $1
  qm set $1 --name $4
}

# main functions
if [ -z "${a}" ]; then
  check_deps virt-customize
  if [ $os == 'ubuntu' ]
  then
    image_name="ubuntu.img"
    template_name="ubuntu-2004"
    pull_image $os $image_name
  elif [ $os == 'centos' ]
  then
    echo "centos image"
    image_name="centos.img"
    template_name="centos-8"
    pull_image $os $image_name
  fi
  customize $image_name
  create_template $vmid $image_name $storage $template_name
fi
