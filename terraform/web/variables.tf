variable "pm_user" {
  description = "proxmox ci user"
  type        = string
}

variable "pm_password" {
  description = "proxmox ci user's password"
  type        = string
}

variable "pm_name" {
  description = "name or ip of proxmox, for instance pve-01.quixada.ifce.edu.br"
  type        = string
}

variable "authorized_keys" {
  description = "list of authorized keys to access instance"
  type        = list(string)
}
