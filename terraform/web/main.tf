module "web-node" {
  source          = "ovitor/instance/proxmox"
  version         = "0.5.6"
  instances       = local.instances
  authorized_keys = local.authorized_keys
}
