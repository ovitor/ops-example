provider "proxmox" {
  pm_tls_insecure = true
  pm_api_url      = format("https://%s:8006/api2/json", var.pm_name)
  pm_user         = var.pm_user
  pm_password     = var.pm_password
}
