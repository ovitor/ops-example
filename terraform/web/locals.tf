locals {
  instances = {
    "web.ifce.local" = {
      description   = "a simple test web node"
      environment   = "dev"
      target_node   = "pve-01"
      template_name = "ubuntu-2004"
      username      = "ubuntu"
      password      = random_string.password.result
      cores         = "2"
      sockets       = "2"
      memory        = "4096"
      disks = [
        {
          size    = "30G"
          storage = "local-lvm"
        }
      ]

      networks = [
        {
          bridge = "vmbr0"
          tag    = "100"
          ip     = "172.16.0.45/21"
          gw     = "172.16.0.1"
        }
      ]

      nameserver   = "172.16.0.1"
      searchdomain = "quixada.ifce.edu.br"

    }
  }

  authorized_keys = <<EOT
%{~for key in data.local_file.authorized_users_keys~}
${key.content}
%{~endfor~}
EOT



}
