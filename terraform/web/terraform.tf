terraform {
  required_version = "1.0.0"
  backend "http" {}

  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.3"
    }
  }
}
