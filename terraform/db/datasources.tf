data "local_file" "authorized_users_keys" {
  for_each = toset(var.authorized_keys)
  filename = "${path.module}/../../public_keys/${each.value}.pub"
}

resource "random_string" "password" {
  length  = 16
  special = false
}
