variable "bw_email" {
  description = "bitwarden email"
  type        = string
}

variable "bw_password" {
  description = "bitwarden password"
  type        = string
}

variable "bw_client_id" {
  description = "bitwarden client_id"
  type        = string
}

variable "bw_client_secret" {
  description = "bitwarden client_secret"
  type        = string
}

variable "bw_folders" {
  description = "bitwarden folders that will be created"
  type        = list(string)
}
