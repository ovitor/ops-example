resource "bitwarden_folder" "servers" {
  for_each = var.bw_folders
  name     = each.key
}
