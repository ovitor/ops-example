terraform {
  required_version = "1.0.0"
  backend "http" {}

  required_providers {
    bitwarden = {
      source  = "maxlaverse/bitwarden"
      version = "0.0.2"
    }
  }
}
