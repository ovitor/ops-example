# ops-example

Esse repositório contém uma estrutura de diretórios exemplo de como
é utilizado por nós. A estrutra é basicamente a seguinte:

```
.
├── create-vm-template.sh
├── envs.sh
├── inventory.yml
├── public_keys
│   ├── adriana.almeida.pub
│   └── vitor.carvalho.pub
├── README.md
├── terraform
│   ├── db
│   │   ├── datasources.tf
│   │   ├── db.yml
│   │   ├── get_state.sh
│   │   ├── locals.tf
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   ├── provider.tf
│   │   ├── README.md
│   │   ├── terraform.tf
│   │   ├── variables.auto.tfvars
│   │   └── variables.tf
│   └── web
│       ├── datasources.tf
│       ├── get_state.sh
│       ├── locals.tf
│       ├── main.tf
│       ├── outputs.tf
│       ├── provider.tf
│       ├── README.md
│       ├── terraform.tf
│       ├── variables.auto.tfvars
│       ├── variables.tf
│       └── web.yml
└── test.yml
```
